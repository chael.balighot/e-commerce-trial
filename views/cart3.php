<?php session_start(); ?>

<?php
# =============================================
# =           Removing Product from Cart             =
# =============================================

# ======  End of Removing Product from Cart    =======
?>

<?php
    $title = "Cart";
    include "../partials/template.php";
?>

<?php function get_content() {; ?>
     <?php
     # ====================================================================
     # =           Loop Through and display contents of SESSION["cart"]           =
     # ====================================================================
      if(isset($_SESSION["cart"])){

               // echo "<pre>";
               // print_r($_SESSION["cart"]);
               // echo "</pre>";

          require "../controllers/connect.php";

          $sql = "select * from items";
          $result = mysqli_query($conn, $sql);
          $product_row = "";

           while($row = mysqli_fetch_assoc($result)){
               foreach($_SESSION["cart"] as $key =>$value ){
                    if($row["id"] == $value["product_id"]){
                         $product_row .= "
                         <tr>
                         <td><img width = '100' height = '100' src=' " . $row["img_path"]. " '></td>
                         <td>" . $row["name"]. "</td>
                         <td>" . $row["price"]. "</td>
                         <td><input data-price='$row[price]' class='qty form-control' min='1' type = 'number' value='". $value["quantity"]."' min='1' max='99'></td>
                         <td id='output'>" . $value["quantity"] * $row["price"]. "</td>
                         <td><a href='cart2.php?product_pos=$key' class='btn btn-danger' name='remove'>Remove</a></td>
                         </tr>";
                    }
                }
          }
          $heading = "My Cart";

     }else{
          $heading = "Cart is Empty";
          $product_row = "";
     }

     # ======  End of Loop Through and display contents of SESSION["cart"]  =======
     ?>



<div class="container mt-5">
     <h2><?php echo $heading ;?></h2>

     <div class="row">
      <!-- 1st Column for Displaying the Contents of the Cart -->
          <div class="col-md-9">
               <table class="table">
                   <thead class = "thead-light">
                     <tr>
                       <th></th>
                       <th>Product</th>
                       <th>Price</th>
                       <th>Quantity</th>
                       <th>Total</th>
                       <th>Remove</th>
                     </tr>
                   </thead>
                   <tbody>
                     <?php
                         echo $product_row;
                     ?>
                   </tbody>
                 </table>
          </div>
          <!-- End of 1st Column for Displaying the Contents of the Cart -->

          <!-- 2nd Column for Displaying Cart Summary -->
          <div class="col-md-3">
               <div class="card">
                 <div class="card-header"><h4>Order Summary</h4></div>
                 <div class="card-body">
                      <h5>Subtotal: <span id="subtotal" class="display-5"></span></h5>
                     <h5>Shipping:<span class="text-success">FREE</span> </h5>
                      <h5>Total: <br><span id="total" class="display-4"> ₱ </span></h5>
                 </div>
                 <div class="card-footer">
                      <a href="checkout.php" class="btn btn-info btn-block">Checkout</a>
                 </div>
               </div>
          </div>
          <!-- End of 2nd Column for Displaying Cart Summary -->

     </div>
</div>

<?php }; ?>

<script type="text/javascript">

      // Run the SubTotal() function  for side bar summary

      // Get all the <td><input class='qty form-control' and store it in qtys
    let qtys = document.querySelectorAll(".qty");

     // Loop through each individual <input class='qty form-control' and attach an EventListener that calls the updateQty function
    for(qty of qtys){
      qty.addEventListener("change", updateQty);
    }

/*=============================================
=            Update The Cart  Function       =
=============================================*/
     function updateQty(){
                // Get the Product Quantity
               let newQty = this.value;
               console.log(`the product quantity is: ${newQty}`);
               // Get the Product ID

               // Get the product position in the SESSION["cart"][product position]

              // Get the Product Price
              let price = this.getAttribute("data-price");
              console.log(`the product price is: ${price}`);
               // New Price newQty * price

               // Get the nextElementSibling. This is where we are going to write the updatedValue
               //<td id='output'>" . $value["quantity"] * $row["price"]. "</td>

               //Write the updatedValue in <td id='output'>;

               //Call the Sub total function

               /*----------  Update the SESSION["cart"] using Fetch API ----------*/

               //  Create Form Data to hold the values to pass to ../controllers/update.php

               // let formData = new FormData();
               // formData.append("qty", newQty);
               // formData.append("product_pos", product_pos);
               // formData.append("product_id", product_id);

               // fetch("../controllers/update.php", {
               //  method: 'POST',
               //  body: formData
               // })
               // .then(function (response){
               //           return response.text();
               // }).then(function(text){
               //           console.log(text);
               // }).catch(function (error){
               //           console.log(error)
               // })

               /*---------- End of Update  SESSION["cart"] using Fetch API ----------*/
     }

/*=====  End of Section comment block  ======*/

/*=============================================
=            Compute The Subtotal  Function of all Products      =
=============================================*/

     function subTotal(){

     }
/*=====  End of Section comment block  ======*/

</script>