<?php $title = "Registration Form";?>
<?php include "../partials/template.php"; ?>
<?php function get_content(){ ?>




<div class="container m-5" >
  <form action="../controllers/register_action.php" method="POST" class="m-5">
    <div class="form-group">
      <label for="fname">First Name:</label>
      <input type="text" class="form-control" id="fname" name="fname" required="">
    </div>
    <div class="form-group">
      <label for="lname">Last Name:</label>
      <input type="text" class="form-control" id="lname" name="lname" required="">
    </div>
    <div class="form-group">
      <label for="address">Address:</label>
      <input type="text" class="form-control" id="address" name="address" required="">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email address:</label>
      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required="">
      
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password:</label>
      <input type="password" class="form-control" id="exampleInputPassword1" name="pw" required="">
    </div>
    

   
    <button type="submit" class="btn btn-primary rounded">Register</button>
  </form>
</div>


<?php } ?>