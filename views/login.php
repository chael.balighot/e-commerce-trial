<?php $title = "Login Form";?>
<?php include "../partials/template.php" ?>
<?php function get_content(){ ?>

	<div class="container" >
		<h1>LOGIN</h1>
		<form method="POST" action="../controllers/login_action.php" class="mt-5">
		  <div class="form-group mt-5 mb-3">
		    <label for="uname">Username:</label>
		    <input type="text" class="form-control" id="uname" name="uname">
		  </div>
		  <div class="form-group mt-5 mb-3">
		    <label for="pw">Password:</label>
		    <input type="password" class="form-control" id="pw" name="pw">
		  </div>
		  <button type="submit" class="btn btn-primary mb-5 rounded mt-5">Submit</button>
		</form>
		
	</div>

<?php } ?>





