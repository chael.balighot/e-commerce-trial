<?php session_start(); ?>
<!-- step 4 -->
<?php $title = "Home"; ?>
<!-- step 5 -->
<?php include "../partials/template.php"; ?>
<?php function get_content(){ ?>
<!-- Insert Jumbotron STEP 6 -->
<div class="jumbotron">
<div class="container">
<h1 class="display-3">The DEMO Shop</h1>
<p>
This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something unique.
</p>
<p>
<a href="catalog.php" class="btn btn-outline-primary">Shop Now</a>
</p>
</div>
</div>

<!-- Featured Items STEP 7 -->
<div class="container">
  <!-- Page Features -->
  <h2>Featured Items</h2>
  <hr>
  <div class="row text-center">
    <!-- Retrieve records from products table and display here using
      class "card" in bootstrap
      -->
    <?php
      require "../controllers/connect.php";
      $sql = "SELECT * FROM items limit 4";
      $result = mysqli_query($conn,$sql);
      
      if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
          echo "
          <div class='col-md-3 mb-3'>
      
            <div class='card h-100'>
               <img src='$row[img_path]'>
                  <div class='card-body'>
                    <h4 class='card-title'>$row[name]</h4>
                    <h5>₱ $row[price]</h5>
      
                  <a href='product.php?id=$row[id]' class='btn btn-block btn-primary'> View Product</a>
                  </div>
      
            </div>
          </div>";
        }
      }
      ?>
  </div>
</div>
<!-- /.row -->
</div>
<!-- /.container -->

<?php } ?>