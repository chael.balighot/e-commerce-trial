<?php session_start(); ?>


<!-- Add to cart -->
<?php 
if (isset($_POST["qty"])) {
	if(isset($_SESSION["cart"])){
		$item_array = array(
			"quantity" => $_POST["qty"],
			"product_id" => $_GET["id"]
		);


		// check if product already exist
		$item_array_id = array_column($_SESSION["cart"], "product_id");

		// check if the product id is in the list id's
		if (in_array($_GET["id"], $item_array_id)) {
			echo "<script>alert('product already exist')</script>";
		}else{
			//$count = count($_SESSION["cart"]);
			// $_SESSION["cart"][] = $item_array;

			$_SESSION["cart"][$_GET["id"]] = $item_array;
			// echo "<pre>";
			// print_r($_SESSION["cart"]);
			// echo "</pre>";
		}
		
		// echo "<pre>";
		// 	print_r($_SESSION["cart"]);
		// echo "</pre>";	

		
	}else{
		$item_array = array(
			"quantity" => $_POST["qty"],
			"product_id" => $_GET["id"]
		);

		
		$_SESSION["cart"][0] = $item_array;
		// echo "<pre>";
		// 	print_r($_SESSION["cart"]);
		// echo "</pre>";	
	}
}
?>


<!-- step 4 -->
<?php $title = "Catalog"; ?>
<!-- step 5 -->
<?php include "../partials/template.php"; ?>
<?php function get_content(){ ?>
<!--Content-->
<div class="container">
  <div class="row">
  	<!-- first col -->
    <div class="col-lg-2">
      <h4 id="catalogue">Collection</h4>
		<div class="list-group">
	      <?php
	      	// Require connection details
	      	require "../controllers/connect.php";
	      	// Retrieve categories
	      	$sql = "SELECT * FROM categories";
	      	$result = mysqli_query($conn,$sql);

	      	if(mysqli_num_rows($result)>0){
	      		while ($row = mysqli_fetch_assoc($result)) {
	      			echo"<a class='list-group-item list-group-item-action'
	      			href='catalog.php?cat_id=$row[id]'>$row[name]</a>";

	      			


	      			
	      		}
	      	}
	      ?>
		</div>
    </div>
    <!-- second col -->
    <div class="col-lg-10">
      <div class="form-group">
        <div class="form-group">
          <div class="input-group mb-3">
            <input type="text" class="form-control" id="search">
            <div class="input-group-append">
              <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
          </div>
        </div>
        </div>
		      <!-- 2nd -->
		<div class="row">
			<?php
			require "../controllers/connect.php";
			
			if(isset($_GET["cat_id"])){
				$cat_id = $_GET["cat_id"];
				$sql = "SELECT * FROM items WHERE category_id = '$cat_id'";
			}else{
				$sql = "SELECT * FROM items";
			}




			
			$result = mysqli_query($conn,$sql);

				if(mysqli_num_rows($result) > 0){
					while($row = mysqli_fetch_assoc($result)){
					echo "

						<div class='col-md-4 mb-3 d-inline-block'>

							<div class='card h-100'>
							<img src='$row[img_path]'>
								<div class='card-body'>
								<h4 class='card-title'><a href='product.php?id=$row[id]'>$row[name]</a></h4>
								<h5>₱ $row[price]</h5>
								<hr>
								<a href='product.php?id=$row[id]' class='btn btn-block btn-primary'> Buy now</a>
								</div>
							</div>
						</div>";
					}
				}

				
			?>
		</div>
    </div>
  </div>
</div>





<?php } ?>