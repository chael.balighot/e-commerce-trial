<?php session_start(); ?>

<?php $title = "Cart";
include "../partials/template.php";


?>
<?php function get_content(){ ?>

  <div class="container">
    <h1>My Cart</h1>
    <!-- <pre>
      <?php
        // print_r($_SESSION["cart"]);
      ?>

      <?php
        // require "../controllers/connect.php";

        // $sql = "SELECT * FROM items";
        // $result = mysqli_query($conn,$sql);

        // while($row = mysqli_fetch_assoc($result)){
        //  foreach ($_SESSION["cart"] as $id) {
        //    if($row["id"] == $id["product_id"]){
        //      echo "ID:".$row["id"]."<br>";
        //      echo "Name:".$row["name"]."<br>";
        //      echo "Price:".$row["price"]."<br>";
        //      echo "Quantity:".$id["quantity"];
        //      echo "<br>";
        //      echo "<hr>";
        //    }
        //  }

        // }

      ?>
    </pre> -->

    <table class="table">
       <thead>
         <tr>
           <th>Image</th>
           <th>Product</th>
           <th>Price</th>
           <th>Quantity</th>
           <th>Total</th>
           <th>Action</th>
         </tr>
       </thead>
       <tbody>
         
          <?php

            if(isset($_SESSION["cart"])){


            require "../controllers/connect.php";

            $sql = "SELECT * FROM items";
            $result = mysqli_query($conn,$sql);

            while($row = mysqli_fetch_assoc($result)){
              foreach ($_SESSION["cart"] as $id => $value) {
                if($row["id"] == $value["product_id"]){
                  // echo "ID:".$row["id"]."<br>";
                  // echo "Name:".$row["name"]."<br>";
                  // echo "Price:".$row["price"]."<br>";
                  // echo "Quantity:".$id["quantity"];
                  // echo "<br>";
                  // echo "<hr>";
                  echo "
                    <tr>
                      <td><img src='$row[img_path]' width='90px'  height='90px'></td>
                      <td>$row[name]</td>
                      <td class='price'>$row[price]</td>
                      <td><input type='number' name='quantity'  class='qty form-control' value='$value[quantity]'></td>
                      <td class='sub'>P".$row['price'] * $value['quantity']."</td>
                      <td><a href='../controllers/delete.php?prod_id=$id' class='btn btn-danger rounded m-2 p-2'>Remove</a></td>
                    </tr>
                  ";
                }
              }

            }
            }else{
              echo "<tr>
                  <td colspan='7' align='center'>**No items Selected**</td>
                </tr>";
            }

        ?>
           
         
       </tbody>
     </table>

  </div>
  <div class="container">
    <div class="row">
      <h2 id="total" class="ml-auto">TOTAL:</h2>
    </div>
  </div>

  <script type="text/javascript">

         let qty = document.getElementsByClassName("qty");
      let price = document.getElementsByClassName("price");
      let sub = document.getElementsByClassName("sub");
      let final  = document.getElementById("total");

      function getTotals(){
         for(i = 0; i < qty.length; i++){
             let subtotal = parseFloat(qty[i].value) * parseFloat(price[i].innerHTML);
             sub[i].innerHTML = subtotal.toFixed(2);
         }
         let x = sub.length;
         let sum = 0;
         while(x--){
             if(qty[x].value == ""){
                 sub[x].innerHTML = 0.00.toFixed(2);
                 let total = sum += parseFloat(sub[x].innerHTML); 
                 final.innerHTML = "Total: P" + total.toFixed(2);
             }else{
                 let total = sum += parseFloat(sub[x].innerHTML);        
                 final.innerHTML = "Total: P" + total.toFixed(2);
             }
         }
      }

      getTotals();

      for(let i = 0; i < qty.length; i++){
         qty[i].addEventListener("change", getTotals);
      }

          




  </script>











<?php } ?>

