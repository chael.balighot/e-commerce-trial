<?php session_start(); ?>

<?php
if(isset($_GET["product_pos"])){
  $product_pos = $_GET["product_pos"];
  // echo $product_pos;
  unset($_SESSION["cart"][$product_pos]);
}

?>


<?php
   $title = "Cart";
   include "../partials/template.php";
?>

<?php function get_content() {; ?>



    <?php

         if(isset($_SESSION["cart"])){
              // echo "<pre>";
              // print_r($_SESSION["cart"]);
              // echo "</pre>";
         require "../controllers/connect.php";

         $sql = "select * from items";
         $result = mysqli_query($conn, $sql);
         $product_row = "";

          while($row = mysqli_fetch_assoc($result)){
              foreach($_SESSION["cart"] as $key =>$value ){
                   if($row["id"] == $value["product_id"]){
                        $product_row .= "
                        <tr>
                        <td><img width = '100' height = '100' src=' " . $row["img_path"]. " '></td>
                        <td>" . $row["name"]. "</td>
                        <td>" . $row["price"]. "</td>
                        <td><input class='qty form-control' data-product-id ='$value[product_id]' data-price = '$row[price]' type = 'number' value='". $value["quantity"]."'></td>
                        <td id='output'>" . $value["quantity"] * $row["price"]. "</td>
                        <td><a href='cart.php?product_pos=$key' class='btn btn-danger' name='remove'>Remove</a></td>
                        </tr>";
                   }
               }
         }
    }else{
         echo "Cart is Empty";
    }
    ?>



<div class="container mt-5">

    <h2>My Cart</h2>

    <div class="row">
      <div class="col-md-8">
          <table class="table">
              <thead class = "thead-light">
                <tr>
                  <th></th>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Sub-Total</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    echo $product_row;
                ?>

              </tbody>
            </table>
      </div>

      <div class="col-md-4">
          <div class="card">
            <div class="card-header"><h4>Order Summary</h4></div>
            <div class="card-body">
                 <h5>Subtotal: <span id="subtotal" class="display-5"></span></h5>
                <h5>Shipping:<span class="text-success">FREE</span> </h5>
                 <h5>Total: <span id="total" class="display-4"> ₱ </span></h5>
            </div>
            <div class="card-footer">
                 <a href="checkout.php" class="btn btn-info btn-block">Checkout</a>
            </div>
          </div>
      </div>

    </div>


</div>

<script type="text/javascript">
  let qtys = document.querySelectorAll(".qty");

  for(qty of qtys){
    qty.addEventListener("change", updateQty);
  }
  function updateQty(){
    let newQTY = this.value;
    let price = this.getAttribute("data-price");

    let updatedValue = newQTY * price;
    console.log(updatedValue);

    let parent = this.parentNode;
    parent.nextElementSibling.innerHTML = updatedValue;
    subTotal();

    fetch("../controllers/update.php",
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
          },
          body: JSON.stringify({qty:newQty})
        }
      ).then(
        function(response){
        return response.text();
        }
      ).then(
        function(text){
        console.log(text);
        }
      ).catch(
        function(error){
        console.log(error);  
        }
      )

  }
  
 
  function subTotal(){
    let productsSubTotal = document.querySelectorAll(".output");
    let sum = 0;
    for(let i = 0; i < productsSubTotal.length; i++){
      sum = sum + parseFloat(productsSubTotal[i].innerHTML);
    }

    document.getElementById("subtotal").innerHTML = "PHP" + sum;
    document.getElementById("total").innerHTML = "PHP" + sum;
  }

</script>

<?php }; ?>