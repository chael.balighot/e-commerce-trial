<!DOCTYPE html>
<html>
   <head>
      <title>Article Manager API</title>
      <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/flatly/bootstrap.min.css">
   </head>
   <body>
      <div class="container">
         <div class="col-lg-12 text-center">
            <h2 class="mt-5">Articles</h2>
            <p class="lead">
               <button class="btn btn-success" id="getArticlesBtn"> Get Articles</button>
            </p>
         </div>
         <div id="articles">
            <ul id="items" class="list-group">
            	
            </ul>
         </div>
      </div>
      <script src="https://code.jquery.com/jquery-3.4.1.min.js"
         integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
         crossorigin="anonymous"></script>
   </body>
</html>

<script type="text/javascript">

	let getArticlesBtn = document.querySelector("#getArticlesBtn");

	// Attach an Event Listener
	getArticlesBtn.addEventListener("click", function(){
		fetch("https://jsonplaceholder.typicode.com/posts")
		.then(function(res){
			return res.json();

		})
		.then(function(data){

			let output = "";
			data.forEach(function(item){
				output += 
				`<li class = "list-group-item">
					<strong>${item.title}</strong>
					${item.body}
				</li>`;
			})
			document.getElementById("items").innerHTML = output;
		
			// let num = 0
			// for (let i = 0; i < data.length; i++) {
			// itemsL.innerHTML += data[i].title + " -----> " + data[i].body + "<hr>"

			// }
			
		})
	})
</script>