<!DOCTYPE html>
<html>
    <head>
        <title>Demo</title>
        <!-- jQuery -->
        
    </head>
    <body>
        <h1>Add Product</h1>
        <form method="POST" action="#">
            <label for="productName">Type Product: </label>
            <input type="text" name="productName" id="productName">
            <span id="feedback"></span>
        </form>

    

       
    </body>
</html>
<script type="text/javascript">
	 	let productName=document.querySelector("#productName");
	 	// console.log(productName);

	 	const formData = new FormData(); 

	 	productName.addEventListener("keyup", function(){
	 		let productNameValue = this.value;
	 		// console.log(productNameValue);

	 		formData.append("productNameValue", productNameValue);

	 		//Fetch API
	 		fetch("items.php", {
	 			method: "POST", 
	 			body:formData

	 		})
	 		.then(function(response){
	 			return response.text()
	 			
	 		})
	 		.then(function(text){
	 			console.log(text)
	 			document.querySelector("#feedback").innerHTML = text
	 		})
	 		.catch(function(error){
	 			console.log(error)
	 		})

	 	});
	</script>